package ee.ut.math.tvt.salessystem.ui;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.HibernateSalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.TeamInfo;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.PurchaseHistory;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * A simple CLI (limited functionality).
 */
public class ConsoleUI {
    private static final Logger log = LogManager.getLogger(ConsoleUI.class);

    private final SalesSystemDAO dao;
    private final ShoppingCart cart;
    private final PurchaseHistory history;
	static HibernateSalesSystemDAO service = new HibernateSalesSystemDAO();

    
    public ConsoleUI(SalesSystemDAO dao) {
        this.dao = dao;
        cart = new ShoppingCart(dao);
        history = new PurchaseHistory(dao);
    }

    public static void main(String[] args) throws Exception {
        SalesSystemDAO dao = new HibernateSalesSystemDAO();
        ConsoleUI console = new ConsoleUI(dao);
        console.run();
        log.info("Salesystem CLI started");
        service.close();
    }

    /**
     * Run the sales system CLI.
     */
    public void run() throws IOException {
        System.out.println("===========================");
        System.out.println("=       Sales System      =");
        System.out.println("===========================");
        printUsage();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.print("> ");
            processCommand(in.readLine().trim().toLowerCase());
            System.out.println("Done. ");
        }
    }

    private void showStock() {
    	List<StockItem> stockitem = dao.getStockItems();
        System.out.println("-------------------------");
        for (StockItem si : stockitem) {
            System.out.println(si.getId() + " " + si.getName() + " " + si.getPrice() + " Euro (" + si.getQuantity() + " items)");
        }
        if (stockitem.size() == 0) {
            System.out.println("\tNothing");
        }
        System.out.println("-------------------------");
    }

    private void showCart() {
        System.out.println("-------------------------");
        for (SoldItem si : cart.getAll()) {
            System.out.println(si.getName() + " " + si.getPrice() + " Euro (" + si.getQuantity() + " items)");
        }
        if (cart.getAll().size() == 0) {
            System.out.println("\tNothing");
        }
        System.out.println("-------------------------");
    }
    
    //add item to cart
    private void addItem(String[] c){    	
    	long barCode = Long.parseLong(c[1]);
        int amount = Integer.parseInt(c[2]);
        StockItem item = dao.findStockItem(barCode);
        if (item != null){
        	try{
            	cart.addItem(new SoldItem(item, amount));
            }
            catch (SalesSystemException ex) {
            	System.out.println(ex.getMessage());
            }   
        } else {
        	System.out.println("No stock item with id " + barCode + ".");
        }        
    }
    
    //add product to warehouse
    private void addProduct(String[] c){
    	long barCode = Long.parseLong(c[1]);
    	String name = c[2];
    	for (int i = 3; i < (c.length - 2); i++){
    		name += " " + c[i];
    	}
    	double price = Double.parseDouble(c[c.length - 2]);
    	Integer quantity = Integer.parseInt(c[c.length - 1]);
    	StockItem stockItem = dao.findStockItem(barCode);
    	try {
			dao.saveStockItem(stockItem, barCode, name, quantity, price);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
    }
    
    
    /**
     * Get team info from properties file 
     */
     
	InputStream inputStream;
 
	public void showTeam() throws IOException {
		
		System.out.println("Team name:  " + TeamInfo.getName());
		System.out.println("Team leader: " + TeamInfo.getLeader());
		System.out.println("Team leader email: " + TeamInfo.getEmail());
		System.out.println("Team members: " + TeamInfo.getMembers());
			
	}
	
    private void printUsage() {
        System.out.println("-------------------------");
        System.out.println("Usage:");
        System.out.println("h\t\tShow this help");
        System.out.println("w\t\tShow warehouse contents");
        System.out.println("c\t\tShow cart contents");
        System.out.println("a IDX NR \tAdd NR of stock item with index IDX to the cart");
        System.out.println("a IDX NM PR NR \tAdd product with name NM, index IDX, price PR and quantity NR to the stock");
        System.out.println("p\t\tPurchase the shopping cart");
        System.out.println("r\t\tReset the shopping cart");
        System.out.println("hist\t\tShow purchase history");
        System.out.println("t\t\tShow team info");
        System.out.println("exit\t\tClose");
        System.out.println("-------------------------");
    }

    private void processCommand(String command) throws IOException {
        String[] c = command.split(" ");
        if (c[0].equals("h"))
            printUsage();
        else if (c[0].equals("q"))
            System.exit(0);
        else if (c[0].equals("w"))
            showStock();
        else if (c[0].equals("c"))
            showCart();
        else if (c[0].equals("t"))
            showTeam();
        else if (c[0].equals("hist"))
            history.showHistory();
        else if (c[0].equals("exit"))
        	System.exit(0);
        else if (c[0].equals("p"))
            cart.submitCurrentPurchase();
        else if (c[0].equals("r"))
            cart.cancelCurrentPurchase();
        else if (c[0].equals("a") && c.length == 3) {
            try {	
            	addItem(c);
            } catch (SalesSystemException | NumberFormatException | NoSuchElementException e) {
                log.error(e.getMessage(), e);
            }
        } else if (c[0].equals("a") && c.length > 4){
        	try {
            	addProduct(c);
        	} catch (SalesSystemException | NumberFormatException | NoSuchElementException e) {
                log.error(e.getMessage(), e);
            }
        } else {
            System.out.println("Unknown command");
        }
    }

}
