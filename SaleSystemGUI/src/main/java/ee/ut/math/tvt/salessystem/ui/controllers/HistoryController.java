package ee.ut.math.tvt.salessystem.ui.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;

import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sun.javafx.collections.ObservableListWrapper;

import ee.ut.math.tvt.salessystem.dao.HibernateSalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.History;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.logic.PurchaseHistory;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;

/**
 * Encapsulates everything that has to do with the History tab (the tab
 * labelled "History" in the menu).
 */
@SuppressWarnings("restriction")
public class HistoryController implements Initializable {

	private static final Logger log = LogManager.getLogger(HistoryController.class);
    private final SalesSystemDAO dao;
    static HibernateSalesSystemDAO service = new HibernateSalesSystemDAO();
    List<History> carts = service.getHistory();

    @FXML
    private Button showAll;
    @FXML
    private Button showTen;
    @FXML
    private Button showBetween;
    @FXML
    private DatePicker startDate;
    @FXML
    private DatePicker endDate;
    @FXML
    private TableView<SoldItem> purchaseTableView;
    @FXML
    private TableView<History> historyTableView;

    public HistoryController(SalesSystemDAO dao, ShoppingCart shoppingCart, PurchaseHistory history) {
        this.dao = dao;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // TODO: implement
    	historyTableView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
    	    if (newSelection != null) {
    	    	List<SoldItem> list = new ArrayList<SoldItem>(newSelection.getSoldItems()); 	    	
    	    	purchaseTableView.setItems(new ObservableListWrapper<>(list));
    	    }
    	});
    	
    	log.debug("HistoryController initialized");
    } 
    
    /**
     * Event handler for the <code>show all</code> event.
     */
    @FXML
    protected void showBetweenButtonClicked() {
        log.debug("Show history between selected dates");
        if (carts != null) {
			List<History> between = new ArrayList<History>(); 
        	try{
	        	Date start = Date.valueOf(startDate.getValue());
	        	Date end = Date.valueOf(endDate.getValue());
	    		if(start.compareTo(end)>0){
	    			PurchaseController.ErrorMessage("End date is before start date");
	    		}
	    		else{
	    			List<History> soldcarts = dao.getHistory();  
	    	    	for (History cart: soldcarts){
	    	    		if(end.compareTo(cart.getKuupaev()) >= 0 && start.compareTo(cart.getKuupaev()) <= 0){
	    	    			between.add(cart);
	    	    		}
	    	    	}
		        	historyTableView.setItems(new ObservableListWrapper<>(between));
		            historyTableView.refresh();
	    		}
        	}
        	catch(NullPointerException e){
        		PurchaseController.ErrorMessage("Missing a date");
        	}     	
        }
    }
    @FXML
    protected void showTenButtonClicked() {
        log.debug("Show last ten purchases");
        if (carts != null) {
        	List<History> soldcarts = dao.getHistory();
        	int suurus = soldcarts.size();
        	List<History> kymme = new ArrayList<History>();
        	try{
	        	if (suurus >= 10){ 
	    	    	for (int i = suurus-1; i > suurus-11; i--){
	    	    		kymme.add(soldcarts.get(i));
	    	    	}
	        	}
	        	else{
	        		kymme = (List<History>) ((ArrayList<History>) soldcarts).clone();
	        	}
	        	historyTableView.setItems(new ObservableListWrapper<>(kymme));
	            historyTableView.refresh();
	            purchaseTableView.refresh();
        	}
        	catch(NullPointerException e){
        		PurchaseController.ErrorMessage("No history");
        	}   
        }
    }
    @FXML
    protected void showAllButtonClicked() {
        log.debug("Show all purchase history");
        if (carts != null) {
        	historyTableView.setItems(new ObservableListWrapper<>(dao.getHistory()));
            historyTableView.refresh();
            purchaseTableView.refresh();
        }
    }
    
}
