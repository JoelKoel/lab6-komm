package ee.ut.math.tvt.salessystem.ui.controllers;

import com.sun.javafx.collections.ObservableListWrapper;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.HibernateSalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "Point-of-sale" in the menu). Consists of the purchase menu,
 * current purchase dialog and shopping cart table.
 */
@SuppressWarnings("restriction")
public class PurchaseController implements Initializable {

    private static final Logger log = LogManager.getLogger(PurchaseController.class);

    private final SalesSystemDAO dao;
    private final ShoppingCart shoppingCart;
	static HibernateSalesSystemDAO service = new HibernateSalesSystemDAO();
	List<StockItem> stockitem = service.getStockItems();

    @FXML
    private Button newPurchase;
    @FXML
    private Button submitPurchase;
    @FXML
    private Button cancelPurchase;
    @FXML
    private TextField barCodeField;
    @FXML
    private TextField quantityField;
    @FXML
    private ChoiceBox<String> nameField;
    @FXML
    private TextField priceField;
    @FXML
    private Button addItemButton;
    @FXML
    private TableView<SoldItem> purchaseTableView;
    
    

    public PurchaseController(SalesSystemDAO dao, ShoppingCart shoppingCart) {
        this.dao = dao;
        this.shoppingCart = shoppingCart;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cancelPurchase.setDisable(true);
        submitPurchase.setDisable(true);
        purchaseTableView.setItems(new ObservableListWrapper<>(shoppingCart.getAll()));
        disableProductField(true);
        
        updateProductList();
        
        nameField.addEventHandler(MouseEvent.MOUSE_RELEASED, new EventHandler<MouseEvent>(){
        	public void handle(MouseEvent nf){
        		fillInputsBySelectedStockItem();
        	}  	
        });
        
        nameField.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable,
					String oldValue, String newValue) {
                	log.debug("Product selected");
                    fillInputsBySelectedStockItem(); 
            }
        });
        quantityField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d*")) {
                	quantityField.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
        log.debug("PurchaseController initialized");
    }
        
    
    public void updateProductList(){
    	ObservableListWrapper<StockItem> Products = new ObservableListWrapper<>(dao.getStockItems());
        List<String> names = new ArrayList<String>();
        
        for (StockItem item : Products) {
        	System.out.println(item.getQuantity());
        	if (item.getQuantity() > 0){
        		names.add(item.getName());
        	}           
        }
    	nameField.setItems(FXCollections.observableList(names));
    	log.debug("Updating product list");
    }
    

    /** Event handler for the <code>new purchase</code> event. */
    @FXML
    protected void newPurchaseButtonClicked() {
        log.debug("New sale process started");
        try {
            enableInputs();
            barCodeField.setDisable(true);
            priceField.setDisable(true);
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * Event handler for the <code>cancel purchase</code> event.
     */
    @FXML
    protected void cancelPurchaseButtonClicked() {
        log.debug("Sale cancelled");
        try {
            shoppingCart.cancelCurrentPurchase();
            disableInputs();
            purchaseTableView.refresh();
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * Event handler for the <code>submit purchase</code> event.
     * @throws IOException 
     */
    @FXML
    protected void submitPurchaseButtonClicked() throws IOException {
        log.debug("Sale complete");
        try {
            log.debug("Contents of the current basket:\n" + shoppingCart.getAll());
            shoppingCart.submitCurrentPurchase();
            disableInputs();
            purchaseTableView.refresh();
            updateProductList();
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }

    // switch UI to the state that allows to proceed with the purchase
    private void enableInputs() {
        resetProductField();
        disableProductField(false);
        cancelPurchase.setDisable(false);
        submitPurchase.setDisable(false);
        newPurchase.setDisable(true);
    }

    // switch UI to the state that allows to initiate new purchase
    private void disableInputs() {
        resetProductField();
        cancelPurchase.setDisable(true);
        submitPurchase.setDisable(true);
        newPurchase.setDisable(false);
        disableProductField(true);
    }

    private void fillInputsBySelectedStockItem() {
        StockItem stockItem = getStockItemByName();
        if (stockItem != null) {
            barCodeField.setText(String.valueOf(stockItem.getId()));
            priceField.setText(String.valueOf(stockItem.getPrice()));
        } else {
            resetProductField();
        }
    }

    // Search the warehouse for a StockItem with the product name selected
    // from the drop down menu.
    private StockItem getStockItemByName() {
        try {
            String name = nameField.getValue();
            return dao.findStockItem(name);
        } catch (NumberFormatException e) {
            return null;
        }
    }
    
    /**
     * Add new item to the cart.
     */
    @FXML
    public void addItemEventHandler() {
        // add chosen item to the shopping cart.
        StockItem stockItem = getStockItemByName();
        if (stockItem != null) {
            int quantity;
            try {
                quantity = Integer.parseInt(quantityField.getText());
            } catch (NumberFormatException e) {
                quantity = 1;
            }
            try {
            	shoppingCart.addItem(new SoldItem(stockItem, quantity));
                purchaseTableView.refresh();
            }
            catch (SalesSystemException e) {
            	ErrorMessage(e.getMessage());
                log.error(e.getMessage(), e);
            }
        }
    }

 	// Error message //
    public static void ErrorMessage(String message) {
	    try{
			Alert error = new Alert(Alert.AlertType.ERROR);
			error.setHeaderText("Error");
			error.setContentText(message);
			error.show();
		}
		catch(ExceptionInInitializerError|NoClassDefFoundError e){}
		finally{}
	}
    
    /**
     * Sets whether or not the product component is enabled.
     */
    private void disableProductField(boolean disable) {
        this.addItemButton.setDisable(disable);
        this.barCodeField.setDisable(disable);
        this.quantityField.setDisable(disable);
        this.nameField.setDisable(disable);
        this.priceField.setDisable(disable);
    }

    /**
     * Reset dialog fields.
     */
    private void resetProductField() {
        barCodeField.setText("");
        quantityField.setText("1");
        priceField.setText("");
    }
}
