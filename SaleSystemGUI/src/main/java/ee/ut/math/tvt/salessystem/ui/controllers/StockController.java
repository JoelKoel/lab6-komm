package ee.ut.math.tvt.salessystem.ui.controllers;

import com.sun.javafx.collections.ObservableListWrapper;

import ee.ut.math.tvt.salessystem.ui.controllers.PurchaseController;
import ee.ut.math.tvt.salessystem.dao.HibernateSalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@SuppressWarnings("restriction")
public class StockController implements Initializable {

	private static final Logger log = LogManager.getLogger(StockController.class);
    private final SalesSystemDAO dao;
    private final PurchaseController purchaseController;
	static HibernateSalesSystemDAO service = new HibernateSalesSystemDAO();
    List<StockItem> stockitem = service.getStockItems();

    @FXML
    private Button addProduct;
    @FXML
    private TextField barCodeField;
    @FXML
    private TextField quantityField;
    @FXML
    private TextField nameField;
    @FXML
    private TextField priceField;
    @FXML
    private TableView<StockItem> warehouseTableView;

    public StockController(SalesSystemDAO dao, PurchaseController purchaseController) {
        this.dao = dao;
        this.purchaseController = purchaseController;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        refreshStockItems();
        
        barCodeField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d*")) {
                	barCodeField.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
        
        quantityField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d*")) {
                	quantityField.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
        
        priceField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d*(\\.\\d*)?")) {
                	priceField.setText(newValue.substring(0, newValue.length() - 1));
                }
            }
        });
        
        log.debug("StockController initialized");
    }

    @FXML
    public void refreshButtonClicked() {
        refreshStockItems();
    }
    
    @FXML
    protected void addProductButtonClicked() {
    	try{
	        Long barCode = Long.parseLong(barCodeField.getText());
	        int quantity = Integer.parseInt(quantityField.getText());
	        String name = nameField.getText();
	        double price = Double.parseDouble(priceField.getText());
	        StockItem stockItem = dao.findStockItem(barCode);
	        dao.saveStockItem(stockItem, barCode, name, quantity, price);
	        refreshStockItems(); 
    	}
    	catch(NumberFormatException e){
    		purchaseController.ErrorMessage("Missing information to add product");
    	} catch (IOException e) {
			purchaseController.ErrorMessage(e.getMessage());
		}
     
    }
    
    private void refreshStockItems() {
    	purchaseController.updateProductList();
        warehouseTableView.setItems(new ObservableListWrapper<>(dao.getStockItems()));
        warehouseTableView.refresh();
    }
}
