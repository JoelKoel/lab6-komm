package ee.ut.math.tvt.salessystem.ui.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.TeamInfo;

public class TeamController implements Initializable {
	
	private static final Logger log = LogManager.getLogger(TeamController.class);
	private final SalesSystemDAO dao;

	@FXML
    private ImageView image;
	
	@FXML
    private Label name;
	@FXML
    private Label leader;
	@FXML
    private Label email;
	@FXML
    private Label members;
	
	
	public TeamController(SalesSystemDAO dao) {
        this.dao = dao;
    }

	public void initialize(URL location, ResourceBundle resources) {
		
		try {
			name.setText(TeamInfo.getName());
			leader.setText(TeamInfo.getLeader());
			email.setText(TeamInfo.getEmail());
			members.setText(TeamInfo.getMembers());
			image.setImage(new Image(TeamInfo.getImage()));
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.debug("TeamController initialized");
    }
}
