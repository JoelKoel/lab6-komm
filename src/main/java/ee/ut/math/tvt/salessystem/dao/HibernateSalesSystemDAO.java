package ee.ut.math.tvt.salessystem.dao;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import ee.ut.math.tvt.salessystem.dataobjects.History;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.Warehouse;

    public class HibernateSalesSystemDAO implements SalesSystemDAO {

        private final EntityManagerFactory emf;
        private final EntityManager em;
        static HibernateSalesSystemDAO service = new HibernateSalesSystemDAO();

        public HibernateSalesSystemDAO() {
            // if you get ConnectException/JDBCConnectionException then you
            // probably forgot to start the database before starting the application
            emf = Persistence.createEntityManagerFactory("pos");
            em = emf.createEntityManager();
        }

        // TODO implement missing methods

        public void close() {
            em.close();
            emf.close();
        }

        @Override
        public void beginTransaction() {
            em.getTransaction().begin();
        }

        @Override
        public void rollbackTransaction() {
            em.getTransaction().rollback();
        }

        @Override
        public void commitTransaction() {
            em.getTransaction().commit();
        }

        @Override
        public StockItem findStockItem(long id) {
        	List<StockItem> stockitem = service.getStockItems();
            for (StockItem item : stockitem) {
                if (item.getId() == id)
                    return item;
            }
            return null;
        }
        
        @Override
        public StockItem findStockItem(String name) {
        	List<StockItem> stockitem = service.getStockItems();
            for (StockItem item : stockitem) {
                if (item.getName() == name)
                    return item;
            }
            return null;
        }

        @Override
        public void saveSoldItem(SoldItem item) {
        	beginTransaction();
            em.merge(item);
            commitTransaction();
        }

        @Override
        public void saveStockItem(StockItem stockItem, Long barCode, String name, int quantity, double price) throws IOException {
        	beginTransaction();
        	StockItem item = Warehouse.saveStockItem(stockItem, barCode, name, quantity, price);
        	em.merge(item);
        	commitTransaction();
        }
        
        @Override
		public void updateStockItem(StockItem StockItem, int quantity){
        	beginTransaction();
        	StockItem updatedItem = Warehouse.updateStockItem(StockItem, quantity);
        	em.merge(updatedItem);
            commitTransaction();
    	}
        
        @Override
        public void addCartToHistory(List<SoldItem> List, double sum) {
        	beginTransaction();
        	SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
        	SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss");
        	History h = new History(List, dateFormatter.format(new Date()), timeFormatter.format(new Date()), sum);
            em.merge(h);
            commitTransaction();
        }

		public List<StockItem> getStockItems() {
			return em.createQuery("from StockItem", StockItem.class).getResultList();
		}

		public List<SoldItem> getSoldItems() {
			return em.createQuery("from SoldItem", SoldItem.class).getResultList();
		}

		public List<History> getHistory() {
			return em.createQuery("from History", History.class).getResultList();
		}
    }