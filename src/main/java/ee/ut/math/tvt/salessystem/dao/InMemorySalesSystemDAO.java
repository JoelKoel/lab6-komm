package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.History;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class InMemorySalesSystemDAO implements SalesSystemDAO {

    private final List<StockItem> stockItemList;
    private final List<SoldItem> soldItemList;
    private final List<History> soldCartList;
    private int beginTransactionCount; // variables for checking transactions
    private int commitTransactionCount;
    private boolean commitBeforeBegin;

    public InMemorySalesSystemDAO() {
        List<StockItem> items = new ArrayList<StockItem>();
        items.add(new StockItem(1L, "Lays chips", "Potato chips", 11.0, 5));
        items.add(new StockItem(2L, "Chupa-chups", "Sweets", 8.0, 8));
        items.add(new StockItem(3L, "Frankfurters", "Beer sauseges", 15.0, 12));
        items.add(new StockItem(4L, "Free Beer", "Student's delight", 0.0, 100));
        this.stockItemList = items;
        this.soldItemList = new ArrayList<>();
        this.soldCartList = new ArrayList<History>();
        this.beginTransactionCount = 0;
        this.commitTransactionCount = 0;
        this.commitBeforeBegin = false;
    }

    @Override
    public List<StockItem> getStockItems() {
        return stockItemList;
    }

    @Override
    public StockItem findStockItem(long id) {
        for (StockItem item : stockItemList) {
            if (item.getId() == id)
                return item;
        }
        return null;
    }
    
    @Override
    public StockItem findStockItem(String name) {
        for (StockItem item : stockItemList) {
            if (item.getName() == name)
                return item;
        }
        return null;
    }

    @Override
    public void saveSoldItem(SoldItem item) {
        soldItemList.add(item);
    }

    @Override
    public void saveStockItem(StockItem stockItem, Long barCode, String name, int quantity, double price) throws IOException {
    	beginTransaction();
    	if(name.length() != 0 && barCode > 0 && price > 0 && quantity > 0){
    		if (stockItem != null){
            	if (stockItem.getName().toLowerCase().equals(name.toLowerCase()) && stockItem.getPrice() == price && stockItem.getId() == barCode){
            		stockItem.setQuantity(stockItem.getQuantity() + quantity);
            		System.out.println("Product added to stock");
            	}
            	else {
                	if(stockItem.getName().toLowerCase().equals(name.toLowerCase()) && stockItem.getId() == barCode && stockItem.getPrice() != price){
                		throw new IOException("Product with this name and barcode already in stock but with different price");
            		}
            		else{
            			throw new IOException("Product with this barcode already in stock");
            		}
            	}
            } else {
        		stockItemList.add(new StockItem(barCode, name, "", price, quantity));
    	        System.out.println("New product added to stock");
        	}
    	} else {
    		throw new IOException("Input error");
    	}
    	commitTransaction();
    }
    
    @Override
	public void updateStockItem(StockItem StockItem, int quantity){
    	beginTransaction();
    	List<StockItem> stockitem = stockItemList;
        for (StockItem item : stockitem) {
            if (item.getName() == StockItem.getName())
                item.setQuantity(quantity);
        }
    	commitTransaction();
	}
    
    @Override
    public void addCartToHistory(List<SoldItem> List, double sum) {
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
    	SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss");
        soldCartList.add(new History(List, dateFormatter.format(new Date()), timeFormatter.format(new Date()), sum));
    }
    
    @Override
    public List<History> getHistory() {
		return soldCartList;
	}
    
    @Override
    public void beginTransaction() {
    	if (commitTransactionCount > beginTransactionCount) {
    		//t66tab ainult testimise erijuhul, muidu v6ib beginTransactionCount suurem olla
    		commitBeforeBegin = true;
    	}
    	beginTransactionCount++;
    }

    @Override
    public void rollbackTransaction() {
    }

    @Override
    public void commitTransaction() {
    	commitTransactionCount++;
    }
    
    public int getbeginTransactionCount() {
    	return beginTransactionCount;
    }
    
    public int getcommitTransactionCount() {
    	return commitTransactionCount;
    }
    
    public boolean getcommitBeforeBegin() {
    	return commitBeforeBegin;
    }
    
    public SoldItem findLastHistorySoldItem(String name) {
    	Set<SoldItem> soldItems = soldCartList.get(soldCartList.size() - 1).getSoldItems();
        for (SoldItem item : soldItems) {
            if (item.getName() == name)
                return item;
        }
        return null;
    }
	
}
