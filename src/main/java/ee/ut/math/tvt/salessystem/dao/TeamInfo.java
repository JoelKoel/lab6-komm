package ee.ut.math.tvt.salessystem.dao;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


/**
 * Team info.
 */

public class TeamInfo {

	public final static String file = "application.properties";

    private static Properties props;

    protected static Properties getProperties() throws FileNotFoundException, IOException
    {
    	if(props == null)
    	{
    		props = new Properties();
    		//System.out.println("Attempting to read from file in: "+new File(file).getCanonicalPath());
    		InputStream inputStream = TeamInfo.class.getClassLoader().getResourceAsStream(file);
    		try {
    	    	props.load(inputStream);
    	    	inputStream.close();
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
    	}
    	return props;
    }

    public static String getName() throws FileNotFoundException, IOException
    {
    	String name = getProperties().getProperty("name");
    	return name;
    }	

	public static String getLeader() throws FileNotFoundException, IOException {
		String leader = getProperties().getProperty("leader");
		return leader;
	}

	public static String getEmail() throws FileNotFoundException, IOException {
		String email = getProperties().getProperty("email");
		return email;
	}

	public static String getMembers() throws FileNotFoundException, IOException {
		String members = getProperties().getProperty("members");
		return members;
	}
	
	public static String getImage() throws FileNotFoundException, IOException {
		String image = getProperties().getProperty("image");
		return image;
	}

	public TeamInfo() {
    }
	
}
