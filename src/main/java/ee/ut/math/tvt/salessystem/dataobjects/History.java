package ee.ut.math.tvt.salessystem.dataobjects;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "HISTORY")
public class History {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	@Column(name = "date")
	private String date;
	@Column(name = "time")
	private String time;
	@Column(name = "double")
	private double total;
	@OneToMany(mappedBy = "history")
	private Set<SoldItem> items;

    public Set<SoldItem> getSoldItems() {
        return items;
    }
	
	private Date kuupaev;
	
	public History(){
		
	}
	
	public History(List<SoldItem> items, String date, String time, double total) {
		this.items = new HashSet<SoldItem>(items);
        this.date = date;
        this.time = time;
        this.total = total;
        this.kuupaev = new Date();
    }
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	
	public Date getKuupaev() {
		return kuupaev;
	}
	
	@Override
	public String toString() {
		return "History [date=" + date + ", time=" + time + ", total=" + total
				+ ", items=" + items + "]";
	}
	
}
