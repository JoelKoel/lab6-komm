package ee.ut.math.tvt.salessystem.dataobjects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * Already bought StockItem. SoldItem duplicates name and price for preserving history.
 */
@Entity
@Table(name = "SOLDITEM")
public class SoldItem {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "quantity")
    private Integer quantity;
    @Column(name = "price")
    private double price;
    @ManyToOne
    private History history;

    public SoldItem() {
    }

    public SoldItem(StockItem stockItem, int quantity) {
        this.name = stockItem.getName();
        this.price = stockItem.getPrice();
        this.id = stockItem.getId();
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public double setSum(Integer quantity) {
        return price * ((double) quantity);
    }
    
    public double getSum() {
        return price * ((double) quantity);
    }

    @Override
    public String toString() {
        return String.format("SoldItem{id=%d, name='%s', quantity='%03d', sum='%f'", id, name, quantity, getSum());
    }
}
