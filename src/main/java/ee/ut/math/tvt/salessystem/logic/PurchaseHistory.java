package ee.ut.math.tvt.salessystem.logic;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.History;


/**
 * Purchase history info.
 */

public class PurchaseHistory {
	
	private static final Logger log = LogManager.getLogger(PurchaseHistory.class);

    private final SalesSystemDAO dao;
	
    public PurchaseHistory(SalesSystemDAO dao) {
        this.dao = dao;
        log.info("Purchase history started");
    }
    
	public void showHistory() {
		for (int i = 0; i < dao.getHistory().size(); i++){
    		History hist = dao.getHistory().get(i);
    		System.out.println("Date: " + hist.getDate() + ", Time: " + hist.getTime() + ", Total: " + String.valueOf(hist.getTotal())+ " " + hist.getSoldItems().toString());
		}
	}
	
    public List<History> findAllSoldCarts() {
    	List<History> soldcarts = dao.getHistory();
        return soldcarts;
    }	
}
