package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.SalesSystemException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {
	
	private static final Logger log = LogManager.getLogger(ShoppingCart.class);
    private final SalesSystemDAO dao;
    private final List<SoldItem> items = new ArrayList<>();
    
    public ShoppingCart(SalesSystemDAO dao) {
        this.dao = dao;
    }

    /**
     * Add new SoldItem to table.
     */
    public void addItem(SoldItem item) throws SalesSystemException{
    	if (item.getQuantity() < 0){
    		throw new SalesSystemException("Quantity can't be negative.");
    	} else {
        	for (int i = 0; i < items.size(); i++){
        		SoldItem itemtry = this.items.get(i);
        		if (itemtry.getId() == item.getId()){
        			int kokku = itemtry.getQuantity() + item.getQuantity();
        			// warehouse checking
        			if (kokku <= (dao.findStockItem(itemtry.getName())).getQuantity()){
        				itemtry.setQuantity(kokku);
        			}
        			else {
        				throw new SalesSystemException("Maximum amount exceeded.");
        			}
        			return;
        		}
        	}
        	if (item.getQuantity() <= (dao.findStockItem(item.getName())).getQuantity()){
        		item.setSum(item.getQuantity());
        		items.add(item);
        		log.info("New item added to shopping cart");
    		}
    		else {
    			throw new SalesSystemException("Maximum amount exceeded.");
    		}          
            log.debug("Added " + item.getName() + " quantity of " + item.getQuantity());
    	}
    }

    public List<SoldItem> getAll() {
        return items;
    }

    public void cancelCurrentPurchase() {
        items.clear();
    }
    
    public void submitCurrentPurchase(){
        // note the use of transactions. InMemorySalesSystemDAO ignores transactions
        // but when you start using hibernate in lab5, then it will become relevant.
        // what is a transaction? https://stackoverflow.com/q/974596
    	List<SoldItem> cart = new ArrayList<>();
    	double total = 0;
        for (SoldItem item : items) {
            dao.saveSoldItem(item);
            cart.add(item);
            total += item.getSum();
            int kokku = (dao.findStockItem(item.getName())).getQuantity() - item.getQuantity();
            StockItem stockItem = dao.findStockItem(item.getName());
            stockItem.setQuantity(kokku);
            dao.updateStockItem(dao.findStockItem(item.getName()), kokku);
        }
        if (cart.size()!=0){
        	dao.addCartToHistory(cart, total);
        }
        items.clear();
    }
    
    public SoldItem findSoldItem(long id) {
        for (SoldItem item : items) {
            if (item.getId() == id)
                return item;
        }
        return null;
    }

}
