package ee.ut.math.tvt.salessystem.logic;

import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

public class Warehouse {

	private static final Logger log = LogManager.getLogger(ShoppingCart.class);
    private final SalesSystemDAO dao;
	
	public Warehouse(SalesSystemDAO dao) {
        this.dao = dao;
    }
	
    public static StockItem saveStockItem(StockItem stockItem, Long barCode, String name, int quantity, double price) throws IOException {
    	if(name.length() != 0 && barCode > 0 && price > 0 && quantity > 0){
    		if (stockItem != null){
            	if (stockItem.getName().toLowerCase().equals(name.toLowerCase()) && stockItem.getPrice() == price && stockItem.getId() == barCode){
            		stockItem.setQuantity(stockItem.getQuantity() + quantity);         		
            		System.out.println("Product added to stock");
            		return stockItem;
            	}
            	else {
                	if(stockItem.getName().toLowerCase().equals(name.toLowerCase()) && stockItem.getId() == barCode && stockItem.getPrice() != price){
                		throw new IOException("Product with this name and barcode already in stock but with different price");
            		}
            		else{
            			throw new IOException("Product with this barcode already in stock");
            		}
            	}
            } else {
        		StockItem item = new StockItem(barCode, name, "", price, quantity);
    	        System.out.println("New product added to stock");
    	        return item;
        	}
    	} else {
    		throw new IOException("Input error");
    	}
    }

	public static StockItem updateStockItem(StockItem stockItem, int quantity) {
		StockItem updatedItem = stockItem;
		updatedItem.setQuantity(quantity);
		return updatedItem;
	}  
	
}
