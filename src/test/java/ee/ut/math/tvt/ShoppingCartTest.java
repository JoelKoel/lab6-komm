package ee.ut.math.tvt;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.History;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;


public class ShoppingCartTest {

	private InMemorySalesSystemDAO dao;
	private ShoppingCart cart;
  
  /**
   * Methods with @Before annotations will be invoked before each test is run.
   */
  @Before
  public void setUp() {
    dao = new InMemorySalesSystemDAO();
    cart = new ShoppingCart(dao);
  }

  @Test
  public void testAddingNewItem() throws IOException {
	  assertEquals(cart.getAll().size(), 0); //cart is empty
	  StockItem item = dao.findStockItem(2);
	  cart.addItem(new SoldItem(item, 3));
	  assertEquals(cart.getAll().size(), 1);
  }
  
  @Test
  public void testAddingExistingItem() throws IOException {
	  StockItem item = dao.findStockItem(2);
	  cart.addItem(new SoldItem(item, 3));
	  assertEquals(cart.findSoldItem(2).getQuantity(), 3, 0.0001);
	  cart.addItem(new SoldItem(item, 3));
	  assertEquals(cart.findSoldItem(2).getQuantity(), 6, 0.0001);
  }
  
  @Test(expected=SalesSystemException.class)
  public void testAddingItemWithNegativeQuantity() {
	  StockItem stockItem = dao.findStockItem(2);
	  SoldItem soldItem = new SoldItem(stockItem, -3);
	  cart.addItem(soldItem);
  }
  
  @Test(expected=SalesSystemException.class)
  public void testAddingItemWithQuantityTooLarge() {
	  StockItem stockItem = dao.findStockItem(2);
	  SoldItem soldItem = new SoldItem(stockItem, 10);
	  cart.addItem(soldItem);
  }
  
  @Test(expected=SalesSystemException.class)
  public void testAddingItemWithQuantitySumTooLarge() {
	  StockItem stockItem = dao.findStockItem(2);
	  SoldItem soldItem = new SoldItem(stockItem, 3);
	  cart.addItem(soldItem);
	  SoldItem soldItem2 = new SoldItem(stockItem, 7);
	  cart.addItem(soldItem2);
  }
  
  @Test
  public void testSubmittingCurrentPurchaseDecreasesStockItemQuantity() {
	  assertEquals(dao.findStockItem(2).getQuantity(), 8);
	  StockItem stockItem = dao.findStockItem(2);
	  SoldItem soldItem = new SoldItem(stockItem, 3);
	  cart.addItem(soldItem);
	  cart.submitCurrentPurchase();
	  assertEquals(dao.findStockItem(2).getQuantity(), 5);
  }
  
  @Test
  public void testSubmittingCurrentPurchaseBeginsAndCommitsTransaction() {
	  assertEquals(dao.getbeginTransactionCount(), 0);
	  assertEquals(dao.getcommitTransactionCount(), 0);
	  assertEquals(dao.getcommitBeforeBegin(), false);
	  StockItem stockItem = dao.findStockItem(2);
	  SoldItem soldItem = new SoldItem(stockItem, 3);
	  cart.addItem(soldItem);
	  cart.submitCurrentPurchase();
	  assertEquals(dao.getbeginTransactionCount(), 1);
	  assertEquals(dao.getcommitTransactionCount(), 1);
	  assertEquals(dao.getcommitBeforeBegin(), false);	  
  }
  
  @Test
  public void testCancellingOrderQuanititesUnchanged() {
	  assertEquals(dao.findStockItem(2).getQuantity(), 8);
	  StockItem stockItem = dao.findStockItem(2);
	  SoldItem soldItem = new SoldItem(stockItem, 3);
	  cart.addItem(soldItem);
	  cart.cancelCurrentPurchase();
	  assertEquals(dao.findStockItem(2).getQuantity(), 8);
  }
  
  @Test
  public void testCancellingOrder() {
	  assertEquals(dao.findStockItem(2).getQuantity(), 8);
	  assertEquals(dao.findStockItem(3).getQuantity(), 12);
	  StockItem stockItem = dao.findStockItem(2);
	  SoldItem soldItem = new SoldItem(stockItem, 3);
	  cart.addItem(soldItem);
	  cart.cancelCurrentPurchase();
	  StockItem stockItem2 = dao.findStockItem(3);
	  SoldItem soldItem2 = new SoldItem(stockItem2, 3);
	  cart.addItem(soldItem2);
	  cart.submitCurrentPurchase();
	  assertEquals(dao.findStockItem(2).getQuantity(), 8);
	  assertEquals(dao.findStockItem(3).getQuantity(), 9);
  }
  
  @Test
  public void testSubmittingCurrentOrderCreatesHistoryItem() {
	  assertEquals(dao.getHistory().size(), 0);
	  StockItem stockItem = dao.findStockItem(2);
	  SoldItem soldItem = new SoldItem(stockItem, 3);
	  StockItem stockItem2 = dao.findStockItem(3);
	  SoldItem soldItem2 = new SoldItem(stockItem2, 3);
	  cart.addItem(soldItem);
	  cart.addItem(soldItem2);
	  cart.submitCurrentPurchase();
	  List<History> soldCartList = dao.getHistory();
	  assertEquals(soldCartList.size(), 1); //check if history item is created
	  assertEquals(soldCartList.get(soldCartList.size() - 1).getSoldItems().size(), 2); //check the solditemlist size
	  assertEquals(dao.findLastHistorySoldItem(soldItem.getName()), soldItem); //check correct items
	  assertEquals(dao.findLastHistorySoldItem(soldItem2.getName()), soldItem2);
  }
  
  @Test
  public void testSubmittingCurrentOrderSavesCorrectTime() {
	  assertEquals(dao.getHistory().size(), 0);
	  StockItem stockItem = dao.findStockItem(2);
	  SoldItem soldItem = new SoldItem(stockItem, 3);
	  cart.addItem(soldItem);
	  cart.submitCurrentPurchase();
	  List<History> soldCartList = dao.getHistory();
	  Date aeg = new Date();
	  boolean vahe = true;
	  if((aeg.getTime()-((soldCartList.get(0)).getKuupaev().getTime()))/1000 > 1){
		  vahe = false;
	  }
	  assertEquals(soldCartList.size(), 1); //check if history item is created
	  assertEquals(vahe, true); //check if time difference is bigger than a second
  }
  
  @Test
  public void minuTestListiSuurenemisel() throws IOException {
	  assertEquals(dao.getStockItems().size(), 4);
	  StockItem stockItem = dao.findStockItem(2);
	  dao.saveStockItem(stockItem, 2L, "Chupa-chups", 2, 8.0);
	  assertEquals(dao.getStockItems().size(), 4);
	  dao.saveStockItem(stockItem, 2L, "Chupa-chups", 2, 8.0);
	  assertEquals(dao.getStockItems().size(), 4);
  }
  
  
  
  
}