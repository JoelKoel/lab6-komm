package ee.ut.math.tvt;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;


public class WarehouseTest {

	private InMemorySalesSystemDAO dao;
  
  /**
   * Methods with @Before annotations will be invoked before each test is run.
   */
  @Before
  public void setUp() {
    dao = new InMemorySalesSystemDAO();
  }
  
  @Test
  public void testAddingItemBeginsAndCommitsTransaction() throws IOException {
	  assertEquals(dao.getbeginTransactionCount(), 0);
	  assertEquals(dao.getcommitTransactionCount(), 0);
	  assertEquals(dao.getcommitBeforeBegin(), false);
	  assertEquals(dao.findStockItem(8), null); //toodet ei ole
	  StockItem stockItem = dao.findStockItem(8);
	  dao.saveStockItem(stockItem, 8L, "Kohv", 4, 5.0);
	  assertEquals(dao.findStockItem(8).getPrice(), 5.0, 0.0001);
	  assertEquals(dao.getbeginTransactionCount(), 1);
	  assertEquals(dao.getcommitTransactionCount(), 1);
	  assertEquals(dao.getcommitBeforeBegin(), false);	  
  }

  @Test
  public void testAddingNewItem() throws IOException {
	  assertEquals(dao.findStockItem(8), null); //toodet ei ole
	  StockItem stockItem = dao.findStockItem(8);
	  dao.saveStockItem(stockItem, 8L, "Kohv", 4, 5.0);
	  assertEquals(dao.findStockItem(8).getPrice(), 5.0, 0.0001);
  }
  
  @Test
  public void testAddingExistingItem() throws IOException {
	  StockItem stockItem = dao.findStockItem(2);
	  dao.saveStockItem(stockItem, 2L, "Chupa-chups", 2, 8.0);
	  assertEquals(dao.findStockItem(2L).getQuantity(), 10, 0.0001); //DAO loomisel 8, nuud 10
  }
  
  @Test(expected=IOException.class)
  public void testAddingItemWithNegativeQuantity() throws IOException {
	  StockItem stockItem = dao.findStockItem(2);
	  dao.saveStockItem(stockItem, 2L, "Chupa-chups", -2, 8.0);
  }
  
}